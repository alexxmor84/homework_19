#include <iostream>
#include <string>

class Animal
{   
public:
    virtual void Voice()
    {
        std::cout << " " << std::endl;
    } 
    virtual ~Animal() {}
};


class Dog : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Woof!" << std::endl;
    }
    ~Dog() override {}
};

class Cat : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Meow!" << std::endl;
    }
    ~Cat() override {}
};

class Mouse : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Peeee!" << std::endl;
    }
    ~Mouse() override {}
};

int main()
{
    int i;

    Animal* array[]{ new Dog, new Cat, new Mouse};

    for (i = 0; i < 3; i++)
    {
        array[i]->Voice();
        delete array[i];
    }

    system("pause");

}